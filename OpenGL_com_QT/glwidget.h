#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QOpenGLWidget>
#include <QGLWidget>

class GLWidget : public QGLWidget
{
    Q_OBJECT
public:
    explicit GLWidget(QWidget *parent = 0);

    void initializeGL();
    void paintGL();
    void Movimento();
    void resizeGL(int w, int h);


private:
    float xr =0;
    float yr =0;

};

#endif // GLWIDGET_H
