### Universidade Tuiuti do Parana ####
### Ciência da Computação ###
### Disciplina: Computação Grafica ###

### Exercicios/Trabalhos ###

* Periodo : 06/2017 - 12/2017
* Ambiente : Desktop

### Ferramentas usadas ###

* Linguagem: C/C++
* Framework : OpenGL
* IDE: QtCreator
* Plataforma : Linux/Windows

### Assuntos  ###
* Sistemas de Coordenadas 2D
* Arquitetura do OpenGL
* Primiticas Geometricas
* Projeções
* Pontos,Linhas e Poligonos
* Transformacoes Geometricas 
* Visualização 
* Sistemas de Coordenadas 3D
* Vetor Normal
* Iluminação
* Shader

### Autor  ###

* Alan Azevedo Bancks

### Contato ###

* E-mail: dsalan@hotmail.com