TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle

CONFIG += opengl dynamic


QT += core gui opengl


SOURCES += main.cpp \
    graphic/Draw.cpp \
    bibutil.cpp


win32 {
    INCLUDEPATH += "C:\mingw64\include"
    #LIBS += -L "C:\mingw64\lib" -lglu32 -lglut32 -lopengl32
    LIBS += -L "C:\mingw64\lib" -lglu32 -lglut32 -lopengl32

}

HEADERS += \
    graphic/Draw.h \
    graphic/ObjectPosition.h \
    bibutil.h

unix {
    LIBS += -lglut -lGLU -lGL -ljpeg
}
