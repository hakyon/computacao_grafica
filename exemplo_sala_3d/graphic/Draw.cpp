#include "Draw.h"

void Draw::Walls(char texture_mode)
{

    // Se estivermos desenhando com textura
    // é necessário que a cor seja branca
    if(texture_mode=='t')
        glColor3f(1,1,1);
    else
        glColor3ub(196,210,184);
}

void Draw::loadWall(string wallPath) {
    ifstream wallFile(wallPath);
    string line;

    while(getline(wallFile, line)) {
        istringstream iss(line);

        float textureScaleX, textureScaleY;

        GLfloat transX, transY, transZ,
                rotatA, rotatX, rotatY, rotatZ,
                scaleX, scaleY, scaleZ;

        if (!(iss >>
              transX >> transY >> transZ >>
              rotatA >> rotatX >> rotatY >> rotatZ >>
              scaleX >> scaleY >> scaleZ >>
              textureScaleX >> textureScaleY
        )) {
            throw "Invalid File exception.";
        } // error

    }
}

Draw::Draw(string wallFile) {
    loadWall(wallFile);
}

void Draw::setTextureScale(float x, float y) {
    glMatrixMode(GL_TEXTURE);
    glLoadIdentity();
    glScalef(x,y,1);
    glMatrixMode(GL_MODELVIEW);
}
