TEMPLATE = app
CONFIG += console c++11

QT+= core gui opengl



SOURCES += main.cpp\
        mainwindow.cpp \
    glwidget.cpp

HEADERS  += mainwindow.h \
    glwidget.h

FORMS    += mainwindow.ui

win32 {
    INCLUDEPATH += "C:\mingw64\include"
    LIBS += -L "C:\mingw64\lib" -lglu32 -lglut32 -lopengl32
}

unix {
    LIBS += -lglut -lGLU -lGL
}
