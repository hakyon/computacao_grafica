#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QOpenGLWidget>


class GLWidget : public QOpenGLWidget
{

public:
    GLWidget(QWidget *parent = 0);
    void initializeGL();
    void paintGL();
    void Movimento();
    void resizeGL(int w, int h);


private:
    float xr =0;
    float yr =0;

};

#endif // GLWIDGET_H
