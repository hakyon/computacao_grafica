#include "mainwindow.h"
#include <QApplication>
#include <GL/glut.h>
#include <GL/glu.h>


float xr=0,yr=0;

void display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(0,0,0);

    glBegin(GL_TRIANGLES);
        glVertex2i(150+xr,300+yr);
        glVertex2i(200+xr,200+yr);
        glVertex2i(100+xr,200+yr);
    glEnd();

    glutSwapBuffers();

}

void SpecialKey(int key, int x, int y)
{
    switch (key) {
    case GLUT_KEY_UP:
        yr++;
        glutPostRedisplay();
        break;
    case GLUT_KEY_DOWN:
        yr--;
        glutPostRedisplay();
        break;
    case GLUT_KEY_LEFT:
        xr--;
        glutPostRedisplay();
        break;
    case GLUT_KEY_RIGHT:
        xr++;
        glutPostRedisplay();
        break;
    default:
        break;
    }
}



int main(int argc, char *argv[])
{

    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(800,600);
    glutInitWindowPosition(600,50);
    glutCreateWindow("Exercicio 1 - Usando OpenGL puro");
    glutDisplayFunc(display);


    glClearColor(1,0,0,1);
    gluOrtho2D(0,400,0,400);
    glutSpecialFunc(SpecialKey);

    glutMainLoop();
    //QApplication a(argc, argv); // usando QT junto
    //MainWindow w;
    //w.show();

    return 0;
    //return a.exec();
}


