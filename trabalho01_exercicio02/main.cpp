#include <iostream>
#include <GL/glut.h>
#include <GL/glu.h>

using namespace std;

void display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(1,0,0);

    glBegin(GL_TRIANGLES);
                  /*X */ /*Y */
        glVertex2i(150,250); // teto
        glVertex2i(200,200);
        glVertex2i(100,200);
    glEnd();

    glColor3f(0,0,1);
    glBegin(GL_QUADS);
        glVertex2i(100,100); // paredes
        glVertex2i(200,100);
        glVertex2i(200,200);
        glVertex2i(100,200);
    glEnd();

    glColor3f(1,1,1);
    glBegin(GL_QUADS);
        glVertex2i(160,150); // porta
        glVertex2i(140,150);
        glVertex2i(140,102);
        glVertex2i(160,102);


        glVertex2i(190,142); // janela 1
        glVertex2i(180,142);
        glVertex2i(180,137);
        glVertex2i(190,137);

        glVertex2i(168,142); // janela 2
        glVertex2i(178,142);
        glVertex2i(178,137);
        glVertex2i(168,137);

        glVertex2i(168,135); // janela 3
        glVertex2i(178,135);
        glVertex2i(178,130);
        glVertex2i(168,130);

        glVertex2i(190,135); // janela 4
        glVertex2i(180,135);
        glVertex2i(180,130);
        glVertex2i(190,130);

    glEnd();
    glutSwapBuffers();

}



int main(int argc, char *argv[])
{
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(800,600);
    glutInitWindowPosition(600,50);
    glutCreateWindow("Exercicio 2 - Usando OpenGL puro");
    glutDisplayFunc(display);


    glClearColor(0,0,0,0);
    gluOrtho2D(0,400,0,400);

    glutMainLoop();

    return 0;
}
