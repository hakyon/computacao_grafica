#include <iostream>
//#include <QOpenGLWidget>
#include <GL/glut.h>
#include <GL/glu.h>

using namespace std;


float xr=0,yr=0,zr = 0,zt = 0;
GLfloat angulo = 45.0f;
int refreshmill =1;

void display2()
{
    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();
    glTranslatef(10,10,10);

    glBegin(GL_TRIANGLES);
        glVertex2f(0.0f,10.0f);
        glVertex2f(-10.0f,0.0f);
        glVertex2f(10.0f,0.0f);
    glEnd();
}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(1,0,0);


    //glMatrixMode(GL_MODELVIEW);
    //glLoadIdentity();
    //glRotatef(0.0f,0.0f,-20.0f,1.0f);
    //glTranslatef(0.0f,0.0f,-20.0f);
    //glLoadIdentity();
    //glTranslated(0,0,10);
    //glTranslatef(10,10,10);
    //glRotatef(zt,0.0f,0.0f,1.0f);
    glRotatef(1.0f,0.0f,0.0f,zt);

    //glRotatef(1.0f,zt,0.0f,1.0f);

    glBegin(GL_TRIANGLES);
                  /*X */ /*Y */
        glVertex2i(150+xr,250+yr); // teto
        glVertex2i(200+xr,200+yr);
        glVertex2i(100+xr,200+yr);
    glEnd();

    glColor3f(0,0,1);
    glBegin(GL_QUADS);
        glVertex2i(100+xr,100+yr); // paredes
        glVertex2i(200+xr,100+yr);
        glVertex2i(200+xr,200+yr);
        glVertex2i(100+xr,200+yr);
    glEnd();

    glColor3f(1,1,1);
    glBegin(GL_QUADS);
        glVertex2i(160+xr,150+yr); // porta
        glVertex2i(140+xr,150+yr);
        glVertex2i(140+xr,102+yr);
        glVertex2i(160+xr,102+yr);


        glVertex2i(190+xr,142+yr); // janela 1
        glVertex2i(180+xr,142+yr);
        glVertex2i(180+xr,137+yr);
        glVertex2i(190+xr,137+yr);

        glVertex2i(168+xr,142+yr); // janela 2
        glVertex2i(178+xr,142+yr);
        glVertex2i(178+xr,137+yr);
        glVertex2i(168+xr,137+yr);

        glVertex2i(168+xr,135+yr); // janela 3
        glVertex2i(178+xr,135+yr);
        glVertex2i(178+xr,130+yr);
        glVertex2i(168+xr,130+yr);

        glVertex2i(190+xr,135+yr); // janela 4
        glVertex2i(180+xr,135+yr);
        glVertex2i(180+xr,130+yr);
        glVertex2i(190+xr,130+yr);


    glEnd();
    glutSwapBuffers();


    cout <<"Eixo X :" << xr << " Eixo Y :" << yr << " Eixo Z:" << zr << " Angulo Eixo Z:" << zt <<endl;

}

void SpecialKey(int key, int x, int y)
{
    switch (key) {
    case GLUT_KEY_F1:
        zt++;
        glutPostRedisplay();
        break;
    case GLUT_KEY_F2:
        zt--;
        glutPostRedisplay();
        break;
    case GLUT_KEY_PAGE_UP:
        zr++;
        glutPostRedisplay();
        break;
    case GLUT_KEY_PAGE_DOWN:
        zr--;
        glutPostRedisplay();
        break;
    case GLUT_KEY_UP:
        yr++;
        glutPostRedisplay();
        break;
    case GLUT_KEY_DOWN:
        yr--;
        //glRotated(1,x,y,1);
        glutPostRedisplay();
        break;
    case GLUT_KEY_LEFT:
        xr--;
        //glRotated(1,x,y,1);
        glutPostRedisplay();
        break;
    case GLUT_KEY_RIGHT:
        xr++;
        //glRotated(1,x,y,1);
        glutPostRedisplay();
        break;
    default:
        break;
    }
}



int main(int argc, char *argv[])
{
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(800,600);
    glutInitWindowPosition(600,50);
    glutCreateWindow("Exercicio 3 - Usando OpenGL puro");
    glutDisplayFunc(display);


    glClearColor(0,0,0,0);
    gluOrtho2D(0,400,0,400);
    glutSpecialFunc(SpecialKey);

    cout << "Comandos:" << endl;
    cout << "Rotação : F1 e F2" << endl;
    cout << "Esquerda e Direita: <- e -> " <<endl;
    cout << "Para cima e para baixo: Seta ^ , Seta v " <<endl;

    glutMainLoop();

    return 0;
}
