TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = trabalho01_exercicio03
TEMPLATE = app

SOURCES += main.cpp

win32 {
    INCLUDEPATH += "C:\mingw64\include"
    LIBS += -L "C:\mingw64\lib" -lglu32 -lglut32 -lopengl32
}

unix {
    LIBS += -lglut -lGLU -lGL
}

