#ifndef DRAW_H
#define DRAW_H

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <GL/gl.h>
#include "include/position.h"

using namespace std;

class Draw {
public:
    Draw(string wallFile);
    void Walls(char texture_mode);

private:
    vector<ObjectPosition> walls;
    void loadWall(string wallFile);
    void draw(ObjectPosition obj);
    void setTextureScale(float x, float y);

};


#endif //DRAW_H
