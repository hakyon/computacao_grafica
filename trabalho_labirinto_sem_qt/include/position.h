#ifndef TEST_OBJECTPOSITION_H
#define TEST_OBJECTPOSITION_H


#include <GL/gl.h>

class ObjectPosition {
public:

    float textureScaleX, textureScaleY;

    GLfloat transX, transY, transZ,
            rotatA, rotatX, rotatY, rotatZ,
            scaleX, scaleY, scaleZ;

    ObjectPosition(GLfloat transX,
                   GLfloat transY,
                   GLfloat transZ,
                   GLfloat rotatA,
                   GLfloat rotatX,
                   GLfloat rotatY,
                   GLfloat rotatZ,
                   GLfloat scaleX,
                   GLfloat scaleY,
                   GLfloat scaleZ,
                   float   textureScaleX,
                   float   textureScaleY
    ) {
        this->transX = transX;
        this->transY = transY;
        this->transZ = transZ;

        this->rotatA = rotatA;
        this->rotatX = rotatX;
        this->rotatY = rotatY;
        this->rotatZ = rotatZ;

        this->scaleX = scaleX;
        this->scaleY = scaleY;
        this->scaleZ = scaleZ;

        this->textureScaleX = textureScaleX;
        this->textureScaleY = textureScaleY;
    }
};


#endif //TEST_OBJECTPOSITION_H
