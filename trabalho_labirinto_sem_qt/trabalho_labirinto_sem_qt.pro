TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
#CONFIG -= qt

QT += core gui opengl


SOURCES += \
    src/bibutil.cpp \
    src/main.cpp \
    src/draw.cpp

DISTFILES += \
    texturas/001acajou.jpg \
    texturas/015eyong.jpg \
    texturas/azulejo.jpg \
    texturas/cerro2.jpg \
    texturas/revmur036.jpg \
    texturas/revmur037.jpg \
    material/borracha.mtl \
    material/cadeira2.mtl \
    material/ceu2.mtl \
    material/cuiabomba.mtl \
    material/janela.mtl \
    material/lampada.mtl \
    material/lapis.mtl \
    material/livro.mtl \
    material/mesagrande.mtl \
    material/mesapeq.mtl \
    material/opengl.mtl \
    material/papel1.mtl \
    material/parede.mtl \
    material/porta.mtl \
    material/quadronegro.mtl \
    material/quadronegro2.mtl \
    material/taca.mtl \
    material/taca2.mtl \
    material/vidro.mtl \
    cordenadas/camera.txt \
    cordenadas/sala3d.txt

HEADERS += \
    include/bibutil.h \
    include/draw.h \
    include/position.h

win32 {
    INCLUDEPATH += "C:\mingw64\include"
    #LIBS += -L "C:\mingw64\lib" -lglu32 -lglut32 -lopengl32
    LIBS += -L "C:\mingw64\lib" -lglu32 -lglut32 -lopengl32

}

unix {
    LIBS += -lglut -lGLU -lGL -ljpeg
}

